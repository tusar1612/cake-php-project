<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('emailaddress', 'An email address is required')
            ->requirePresence('password', 'A password is required');
    }

}
